# Guix-jp公式サイト
「GNU Guix及びGNU Guix Systemの日本語コミュニティ」であるGuix-jpの公式サイトです。
  * http://guix-jp.gitlab.io/

## 記事の寄稿
記事の寄稿を受け付けています。
GNU Guix SystemかGNU Guixに少しでも関連するものであれば大丈夫です。
また、Guix-jpサイト内のブログとしての掲載だけでなく、ご自身のブログのリンクの掲載も可能です。

サイト内のブログとして掲載する場合、[後述のライセンス](#ライセンス)の元で公開されることをご留意ください。

### 例
  * GNU Guixにxxxxというパッケージを登録してインストールしてみた
  * GNU Guix Systemにおいてxxxxで日本語が表示できなかったのでxxxxして解決した

## ローカルでのビルド結果の確認
### 必要なもの
  * hugo_extended
  * npm

### ビルド手順
  1. `git clone https://gitlab.com/guix-jp/guix-jp.gitlab.io`
  2. `cd guix-jp.gitlab.io`
  3. `npm install --ignore-scripts`
  4. `hugo serve`

## ライセンス
このレポジトリ及びサイトは[CC-BY-SA 4.0](LICENSE)の元で公開されています。

### テンプレートのライセンス
このレポジトリは[doks-child-theme](https://github.com/h-enk/doks-child-theme)をテンプレートとしています。
[MIT license](notice/doks-child-theme/LICENSE)で公開されていたものを使用しています。

### ロゴのライセンス
`static/`以下にある`favicon.svg`は、[guix-artwork.gitのlogo/Guix.svg](https://git.savannah.gnu.org/cgit/guix/guix-artwork.git/tree/logo/Guix.svg)と同一のものです。
また、`static/`以下にある`apple-touch-icon.png`、`favicon-16x16.png`、`favicon-32x32.png`は`favicon.svg`を各サイズのPNGファイルに変換したものです。

元のファイルは[このような](notice/logo/README)表示が成されていて、[CC-BY-SA 4.0](notice/logo/LICENSE)の元で利用が許可されています。
