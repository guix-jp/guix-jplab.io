---
title : "Guix-jp"
description: "GNU Guix及びGNU Guix Systemの日本語コミュニティ"
lead: "GNU Guix及びGNU Guix Systemの日本語コミュニティ"
draft: false
images: []
---
<section class="section section-sm">
  <div class="container">
    <div class="row justify-content-center text-center">
      <div class="col-lg-8">
        <h2 class="h4">まだ使ったことがない人から玄人まで</h2>
        <p>GNU GuixやGNU Guix Systemに興味があるけれど使ったことがないという人も、長年使ってきた人も、Guix使いと交流したい人なら誰でも歓迎します。</p>
      </div>
      <div class="col-lg-8">
        <h2 class="h4">どんな質問や話題も歓迎</h2>
        <p>どのような質問でも歓迎します。聞いてよいかどうか不安でも躊躇う必要はありません。</p>
      </div>
    </div>
  </div>
</section>
