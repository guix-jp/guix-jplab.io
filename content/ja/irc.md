---
title: "IRCチャンネルについて"
description: "Guix-jpにおけるIRCについての説明。"
draft: false
images: []
---
Guix-jpでは、[The Open and Free Technology Community (OFTC)](https://www.oftc.net/)のIRCチャンネルとSlack、Matrixとのブリッジを行っています。
SlackのチャンネルやMatrixのルームの名前に、`#guix-jp/`という接頭辞をつけたものがチャンネルの名前になっています。

- 接続先: [ircs://irc.oftc.net:6697](ircs://irc.oftc.net:6697)

現状Guix-jpでブリッジしているのは以下のチャンネルになります。
ざっくりとした区分けを一応決めていますが、厳密に話題を振り分ける必要はないです。
ここに書いてある区分けに従っているかどうかはあまり気にせず、気軽に話していただけると幸いです。
- #guix-jp/free-software
  - 自由ソフトウェアについて語るチャンネル
- #guix-jp/general
  - お硬めの連絡系のチャンネル
- #guix-jp/guile
  - GNU Guileについて話すチャンネル
- #guix-jp/guix
  - パッケージマネージャのGNU Guixについて話すチャンネル
- #guix-jp/guix-jp
  -コミュニティの運営などに関してのディスカッションなどを行うチャンネル
  - TODO管理は [GitLab](https://gitlab.com/groups/guix-jp/-/issues)
- #guix-jp/guix-system
  - オペレーションシステムであるGNU Guix Systemについて話すチャンネル
- #guix-jp/question
  - なんでも質問コーナー
- #guix-jp/random
  - トピックにこだわらず好きなことを話すチャンネル
- #guix-jp/test
  - SlackやMatrix、IRCの挙動のテスト用のチャンネル
- #guix-jp/translating
  - Guix のウェブサイトやマニュアルの翻訳に関するチャンネル
- #guix-jp/welcome
  - 自己紹介(したいひとだけでOK)

## チャンネル名コピー用
```
#guix-jp/free-software, #guix-jp/general, #guix-jp/guile, #guix-jp/guix, #guix-jp/guix-jp, #guix-jp/guix-system, #guix-jp/question, #guix-jp/random, #guix-jp/test, #guix-jp/translating, #guix-jp/welcome
```
