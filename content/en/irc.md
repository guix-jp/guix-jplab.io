---
title: "IRC Channel"
description: "Guix-jp on IRC"
draft: false
images: []
---
Guix-jp bridges between IRC channels on [The Open and Free Technology Community (OFTC)](https://www.oftc.net/), Slack and Matrix.
Each channel name is `#guix-jp/NAME` with NAME replaced with the Slack channel name or the Matrix room name.

- Connect to: [ircs://irc.oftc.net:6697](ircs://irc.oftc.net:6697)

The following are the bridged channels.
Feel free to talk here without being confined by classification.

- #guix-jp/free-software
  - Channel for free software
- #guix-jp/general
  - Anouncement
- #guix-jp/guile
  - Channel for GNU Guile
- #guix-jp/guix
  - Channel for GNU Guix, a package manager
- #guix-jp/guix-jp
  -Channel for Guix-jp community management
  - to-do list is managed on [GitLab](https://gitlab.com/groups/guix-jp/-/issues)
- #guix-jp/guix-system
  - Channel for GNU Guix System, a operation system
- #guix-jp/question
  - Channel for question. Ask anything!
- #guix-jp/random
  - Channnel to talk about anything
- #guix-jp/test
  - Channel to test Slack, Matrix and IRC
- #guix-jp/translating
  - Channel for translating Guix website, manual, etc...
- #guix-jp/welcome
  - Channel for self-introduction (only if you want to do)

## For copy channel names
```
#guix-jp/free-software, #guix-jp/general, #guix-jp/guile, #guix-jp/guix, #guix-jp/guix-jp, #guix-jp/guix-system, #guix-jp/question, #guix-jp/random, #guix-jp/test, #guix-jp/translating, #guix-jp/welcome
```
