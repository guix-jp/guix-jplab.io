---
title : "Guix-jp"
description: "Japanese Community of GNU Guix and GNU Guix System"
lead: "Japanese Community of GNU Guix and GNU Guix System"
draft: false
images: []
---
<section class="section section-sm">
  <div class="container">
    <div class="row justify-content-center text-center">
      <div class="col-lg-8">
        <h2 class="h4">From Newcomer to Expert</h2>
        <p>Everyone is welcome, from someone who is interested in GNU Guix and/or GNU Guix System, to expert on them.</p>
      </div>
      <div class="col-lg-8">
        <h2 class="h4">Any question is welcome</h2>
        <p>Any question is welcome.
        You should not hesitate to ask any question, even if you cannot decide whether it is an appropriate question here.</p>
      </div>
    </div>
  </div>
</section>
